# Backend

## Development Setup

$ docker-compose run backend mix ecto.create

$ docker-compose run backend mix ecto.migrate

$ docker-compose up


## TODO

- Add Integration Test for Login and Logout
- Add Logout link on ex_admin dashboard
- Also test return content of json


## Done

- Past Performance Item Test
- Implement api for past performance item with test
- Alter field title in past performance items to field details
- Configure what you see is what you get editor on past performance item's details field
