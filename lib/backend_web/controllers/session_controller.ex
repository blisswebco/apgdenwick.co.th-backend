defmodule BackendWeb.SessionController do
  @moduledoc """
  Controller for Authentication.
  """
  use BackendWeb, :controller

  alias Backend.Session

  def new(conn, _params) do
    render(conn, "new.html")
  end

  def create(conn, %{"session" => session_params}) do
    case Session.login(session_params) do
      {:ok, user} ->
        conn
        |> put_session(:current_user, user.id)
        |> put_flash(:info, "Logged in")
        |> redirect(to: "/admin")

      :error ->
        conn
        |> put_flash(:info, "Wrong username or password")
        |> render("new.html")
    end
  end

  def delete(conn, _) do
    conn
    |> delete_session(:current_user)
    |> put_flash(:info, "Logged out")
    |> redirect(to: "/login")
  end
end
