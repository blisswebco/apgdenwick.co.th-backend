defmodule BackendWeb.Api.PastPerformanceController do
  use BackendWeb, :controller
  alias Backend.Project

  action_fallback(BackendWeb.FallbackController)

  def index(conn, _) do
    past_performances = Project.list_past_performances()

    conn
    |> render("index.json", past_performances: past_performances)
  end
end
