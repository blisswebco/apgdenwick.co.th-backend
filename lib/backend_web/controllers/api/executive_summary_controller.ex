defmodule BackendWeb.Api.ExecutiveSummaryController do
  use BackendWeb, :controller
  alias Backend.Staff
  plug(:scrub_params, "executive_summary" when action in [:create, :update])

  action_fallback(BackendWeb.FallbackController)

  def index(conn, _) do
    executive_summaries = Staff.list_executive_summaries()
    conn |> render("index.json", executive_summaries: executive_summaries)
  end
end
