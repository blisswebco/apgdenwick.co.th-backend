defmodule BackendWeb.Api.PastPerformanceItemController do
  use BackendWeb, :controller
  alias Backend.Project

  action_fallback(BackendWeb.FallbackController)

  def index(conn, _) do
    past_performance_items = Project.list_past_performance_items()

    conn
    |> render("index.json", past_performance_items: past_performance_items)
  end
end
