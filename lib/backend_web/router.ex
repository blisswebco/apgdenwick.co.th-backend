defmodule BackendWeb.Router do
  use BackendWeb, :router
  use ExAdmin.Router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :auth do
    plug(Backend.Auth, %{})
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", BackendWeb do
    # Use the default browser stack
    pipe_through(:browser)

    get("/login", SessionController, :new)
    post("/login", SessionController, :create)
    delete("/logout", SessionController, :delete)
  end

  # Other scopes may use custom stacks.
  scope "/api", BackendWeb do
    pipe_through(:api)

    resources(
      "/executive_summaries",
      Api.ExecutiveSummaryController,
      as: "api_executivesummary",
      only: [:index]
    )

    resources(
      "/past_performances",
      Api.PastPerformanceController,
      as: "api_pastperformance",
      only: [:index]
    )

    resources(
      "/past_performance_items",
      Api.PastPerformanceItemController,
      as: "api_pastperformanceitem",
      only: [:index]
    )
  end

  scope "/admin", ExAdmin do
    pipe_through([:browser, :auth])
    admin_routes()
  end
end
