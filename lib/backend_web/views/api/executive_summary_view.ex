defmodule BackendWeb.Api.ExecutiveSummaryView do
  use BackendWeb, :view

  alias BackendWeb.Api.ExecutiveSummaryView

  def render("index.json", %{executive_summaries: executive_summaries}) do
    %{
      executive_summaries:
        render_many(executive_summaries, ExecutiveSummaryView, "executive_summary.json")
    }
  end

  def render("executive_summary.json", %{executive_summary: executive_summary}) do
    %{
      id: executive_summary.id,
      order: executive_summary.order,
      name: executive_summary.name,
      age: executive_summary.age,
      gender: executive_summary.gender,
      position: executive_summary.position,
      other_info: executive_summary.other_info
    }
  end
end
