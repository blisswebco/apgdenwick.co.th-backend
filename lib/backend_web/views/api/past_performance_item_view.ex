defmodule BackendWeb.Api.PastPerformanceItemView do
  use BackendWeb, :view
  alias BackendWeb.Api.PastPerformanceItemView

  def render("index.json", %{past_performance_items: past_performance_items}) do
    %{
      past_performance_items:
        render_many(past_performance_items, PastPerformanceItemView, "past_performance_item.json")
    }
  end

  def render("past_performance_item.json", %{past_performance_item: past_performance_item}) do
    %{
      id: past_performance_item.id,
      details: past_performance_item.details,
      past_performance_id: past_performance_item.past_performance_id
    }
  end
end
