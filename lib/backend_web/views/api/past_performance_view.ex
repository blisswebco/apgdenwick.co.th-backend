defmodule BackendWeb.Api.PastPerformanceView do
  @moduledoc """
  Past Performance View
  """
  use BackendWeb, :view

  alias BackendWeb.Api.PastPerformanceView

  def render("index.json", %{past_performances: past_performances}) do
    %{
      past_performances:
        render_many(past_performances, PastPerformanceView, "past_performance.json")
    }
  end

  def render("past_performance.json", %{past_performance: past_performance}) do
    %{id: past_performance.id, name: past_performance.name, slug: past_performance.slug}
  end
end
