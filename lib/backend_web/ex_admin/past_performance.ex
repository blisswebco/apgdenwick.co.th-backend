defmodule BackendWeb.ExAdmin.PastPerformance do
  @moduledoc """
  Provides Past Performances Web Management Interface
  """
  use ExAdmin.Register

  register_resource Backend.Project.PastPerformance do
    index do
      selectable_column()
      column(:slug)
      column(:title)
      actions()
    end

    form past_performance do
      inputs do
        input(past_performance, :name)
      end
    end
  end
end
