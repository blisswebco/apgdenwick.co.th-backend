defmodule BackendWeb.ExAdmin.PastPerformanceItem do
  @moduledoc """
  Provides Past Performance Items Web Management
  """
  use ExAdmin.Register
  alias Backend.Project

  register_resource Backend.Project.PastPerformanceItem do
    form item do
      inputs do
        input(item, :past_performance, collection: Project.list_past_performances)
        input(item, :details, type: :text)
      end
    end
  end
end
