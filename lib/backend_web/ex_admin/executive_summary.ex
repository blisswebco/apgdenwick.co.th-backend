defmodule BackendWeb.ExAdmin.ExecutiveSummary do
  @moduledoc """
  Provides Executive Summaries Web Management
  """
  use ExAdmin.Register

  register_resource Backend.Staff.ExecutiveSummary do
    form executive_summary do
      inputs do
        input(executive_summary, :order)
        input(executive_summary, :name)
        input(executive_summary, :age)
        input(executive_summary, :gender)
        input(executive_summary, :position)
        input(executive_summary, :other_info, type: :text)
      end
    end
  end
end
