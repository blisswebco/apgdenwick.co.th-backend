defmodule BackendWeb.ExAdmin.User do
  @moduledoc """
  Provides User Management Web Interface
  """
  use ExAdmin.Register

  register_resource Backend.Account.User do
    form user do
      inputs do
        input(user, :username)
        input(user, :password)
      end
    end
  end
end
