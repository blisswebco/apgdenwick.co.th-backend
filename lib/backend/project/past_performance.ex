defmodule Backend.Project.PastPerformance do
  @moduledoc """
  Past Performance Schema
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Backend.Project.PastPerformance
  alias Backend.Project.PastPerformance.NameSlug
  alias Backend.Project.PastPerformanceItem

  schema "past_performances" do
    field(:name, :string)
    field(:slug, NameSlug.Type)
    has_many(:items, PastPerformanceItem)

    timestamps()
  end

  def changeset(%PastPerformance{} = past_performance, attrs) do
    past_performance
    |> cast(attrs, [:name])
    |> validate_required([:name])
    |> unique_constraint(:name)
    |> NameSlug.maybe_generate_slug()
    |> NameSlug.unique_constraint()
  end
end
