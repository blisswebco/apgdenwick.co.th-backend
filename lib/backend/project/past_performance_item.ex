defmodule Backend.Project.PastPerformanceItem do
  @moduledoc """
  Shema for Past Peformance Items
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Backend.Project.PastPerformanceItem
  alias Backend.Project.PastPerformance

  schema "past_performance_items" do
    belongs_to(:past_performance, PastPerformance)
    field(:details, :string)

    timestamps()
  end

  def changeset(%PastPerformanceItem{} = item, attrs) do
    item
    |> cast(attrs, [:details, :past_performance_id])
    |> validate_required([:details, :past_performance_id])
  end
end
