defmodule Backend.Project.PastPerformance.NameSlug do
  @moduledoc """
  NameSlug.
  Slug field :name and store into :slug field
  """
  use EctoAutoslugField.Slug, from: :name, to: :slug
end
