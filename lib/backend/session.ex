defmodule Backend.Session do
  @moduledoc """
  Session Functionality
  """
  alias Backend.Account
  alias Comeonin.Pbkdf2

  def login(params) do
    user = Account.get_user_by_map(%{username: params["username"]})

    case authenticate(user, params["password"]) do
      true -> {:ok, user}
      _ -> :error
    end
  end

  defp authenticate(user, password) do
    case user do
      nil -> false
      _ -> Pbkdf2.checkpw(password, user.crypted_password)
    end
  end
end
