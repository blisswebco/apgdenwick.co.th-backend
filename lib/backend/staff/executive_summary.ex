defmodule Backend.Staff.ExecutiveSummary do
  @moduledoc """
  Schema for Executive Summaries for Staff
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Backend.Staff.ExecutiveSummary

  schema "executive_summaries" do
    field(:order, :integer)
    field(:name, :string)
    field(:age, :integer)
    field(:gender, :string)
    field(:position, :string)
    field(:other_info, :string)

    timestamps()
  end

  @doc false
  def changeset(%ExecutiveSummary{} = executive_summary, attrs) do
    executive_summary
    |> cast(attrs, [:order, :name, :age, :gender, :position, :other_info])
    |> validate_required([:order, :name, :age, :gender, :position])
    |> unique_constraint(:order)
  end
end
