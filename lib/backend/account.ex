defmodule Backend.Account do
  @moduledoc """
  Provides User Account Related Functions
  """
  import Ecto.Query, warn: false
  alias Backend.Account.User
  alias Backend.Repo

  def list_users do
    User |> Repo.all()
  end

  def create_user(params \\ %{}) do
    %User{}
    |> User.changeset(params)
    |> Repo.insert()
  end

  def update_user(%User{} = user, params \\ %{}) do
    user
    |> User.changeset(params)
    |> Repo.update()
  end

  def get_user_by_map!(%{} = map) do
    Repo.get_by!(User, map)
  end

  def get_user_by_map(%{} = map) do
    Repo.get_by(User, map)
  end

  def delete_user(%User{} = user) do
    Repo.delete(user)
  end
end
