defmodule Backend.Account.User do
  @moduledoc """
  User Schema
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Backend.Account.User
  alias Ecto.Changeset
  alias Comeonin.Pbkdf2

  schema "users" do
    field(:username, :string)
    field(:crypted_password, :string)
    field(:password, :string, virtual: true)

    timestamps()
  end

  @doc false
  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:username, :password])
    |> validate_required([:username, :password])
    |> unique_constraint(:username)
    |> hash_password()
  end

  @doc false
  def hash_password(%Changeset{valid?: false} = changeset), do: changeset

  @doc false
  def hash_password(%Changeset{valid?: true, changes: %{password: password}} = changeset) do
    change(changeset, Pbkdf2.add_hash(password, hash_key: :crypted_password))
  end
end
