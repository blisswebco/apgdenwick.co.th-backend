defmodule Backend.Project do
  @moduledoc """
  Provides Past Performance related Functions
  """
  import Ecto.Query, warn: false
  alias Backend.Project.PastPerformance
  alias Backend.Project.PastPerformanceItem
  alias Backend.Repo

  @doc false
  def list_past_performances do
    PastPerformance |> Repo.all()
  end

  @doc false
  def create_past_performance(params \\ %{}) do
    %PastPerformance{}
    |> PastPerformance.changeset(params)
    |> Repo.insert()
  end

  @doc false
  def update_past_performance(%PastPerformance{} = past_performance, params \\ %{}) do
    past_performance
    |> PastPerformance.changeset(params)
    |> Repo.update()
  end

  @doc false
  def get_past_performance_by_map!(%{} = map) do
    Repo.get_by!(PastPerformance, map)
  end

  @doc false
  def delete_past_performance(%PastPerformance{} = past_performance) do
    Repo.delete(past_performance)
  end

  @doc false
  def list_past_performance_items do
    PastPerformanceItem |> Repo.all()
  end

  @doc false
  def get_past_performance_item_by_map!(%{} = map) do
    Repo.get_by!(PastPerformanceItem, map)
  end

  @doc false
  def create_past_performance_item(params \\ %{}) do
    %PastPerformanceItem{}
    |> PastPerformanceItem.changeset(params)
    |> Repo.insert()
  end

  @doc false
  def update_past_performance_item(past_performance_item, params \\ %{}) do
    past_performance_item
    |> PastPerformanceItem.changeset(params)
    |> Repo.update()
  end

  @doc false
  def delete_past_performance_item(%PastPerformanceItem{} = past_performance_item) do
    Repo.delete(past_performance_item)
  end
end
