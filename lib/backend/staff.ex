defmodule Backend.Staff do
  @moduledoc """
  Provides Staff Related Functions
  """
  import Ecto.Query, warn: false
  alias Backend.Staff.ExecutiveSummary
  alias Backend.Repo

  @doc false

  def list_executive_summaries do
    ExecutiveSummary |> Repo.all()
  end

  @doc false
  def create_executive_summary(params \\ %{}) do
    %ExecutiveSummary{}
    |> ExecutiveSummary.changeset(params)
    |> Repo.insert()
  end

  @doc false
  def update_executive_summary(%ExecutiveSummary{} = executive_summary, params \\ %{}) do
    executive_summary
    |> ExecutiveSummary.changeset(params)
    |> Repo.update()
  end

  @doc false
  def get_executive_summary_by_map!(%{} = map) do
    Repo.get_by!(ExecutiveSummary, map)
  end

  @doc false
  def delete_executive_summary(%ExecutiveSummary{} = executive_summary) do
    Repo.delete(executive_summary)
  end
end
