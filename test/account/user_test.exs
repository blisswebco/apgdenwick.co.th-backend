defmodule Backend.Account.Usertest do
  use Backend.DataCase, async: true

  alias Backend.Account.User
  alias Backend.Account

  @valid_attrs_list [%{username: "admin1", password: "password1"},
		     %{username: "admin2", password: "password2"}
		    ]

  @invalid_attrs_list [%{},
		       hd(@valid_attrs_list) |> Map.delete(:username),
		       hd(@valid_attrs_list) |> Map.delete(:password)
		      ]

  setup do
    [users:
     @valid_attrs_list
     |> Enum.map(fn(valid_attrs) ->
       {:ok, user} = Account.create_user(valid_attrs)
       user
     end)
    ]
  end

  test "valid attrs" do
    Enum.map(@valid_attrs_list,
      fn(valid_attrs) ->
	changeset = %User{} |> User.changeset(valid_attrs)
	assert changeset.valid?
      end
    )
  end

  test "invalid attrs" do
    Enum.map(@invalid_attrs_list,
      fn(attrs) ->
	changeset = %User{} |> User.changeset(attrs)
	refute changeset.valid?
      end
    )
  end

  test "create user with valid data" do
    attrs = %{username: "admin3", password: "password3"}
    assert {:ok , _ } = Account.create_user(attrs)
  end

  test "create user with invalid attrs" do
    attrs = %{}
    assert {:error, _changeset} = Account.create_user(attrs)
  end

  test "update success", %{users: [user | _rest]} do
    attrs = %{username: "username4", password: "password4"}
    assert {:ok, _} = Account.update_user(user, attrs)
  end

  test "update fails when attrs are valid", %{users: [user | _rest]} do
    attrs = %{username: ""}
    assert {:error, _changeset} = Account.update_user(user, attrs)
  end

  test "get_user_by_map!", %{users: [user | _rest]} do
    assert ^user = Account.get_user_by_map!(%{id: user.id})
    assert_raise Ecto.NoResultsError, fn ->
      Account.get_user_by_map!(%{id: 100})
    end
  end

  test "get_user_by_map", %{users: [user | _rest]} do
    assert ^user = Account.get_user_by_map(%{id: user.id})
    assert nil == Account.get_user_by_map(%{id: 100})
  end


  test "deleting user", %{users: [user | _rest]} do
    {:ok, _ } = Account.delete_user(user)
    assert_raise Ecto.NoResultsError, fn -> Account.get_user_by_map!(%{id: user.id}) end
  end
end
