defmodule Backend.Project.PastPerformanceItemTest do
  use Backend.DataCase, async: true

  alias Backend.Project
  alias Backend.Project.PastPerformanceItem

  @valid_attrs %{details: "item1"}
  @invalid_attrs %{}

  setup do
    past_performances = gen_past_performances()
    [past_performances: past_performances]
  end

  test "changeset with valid attributes", %{past_performances: [first_past_performance | _rest]} do
    changeset = PastPerformanceItem.changeset(
      %PastPerformanceItem{},
      Map.put(@valid_attrs, :past_performance_id, first_past_performance.id))
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = PastPerformanceItem.changeset(
      %PastPerformanceItem{},
      @invalid_attrs)
    refute changeset.valid?
  end

  test "create success", %{past_performances: [first_past_performance | _rest]} do
    assert {:ok, _} =
      ( Map.put(@valid_attrs, :past_performance_id, first_past_performance.id)
      |> Project.create_past_performance_item() )
  end

  test "update success",  %{past_performances: [first_past_performance | _rest]} do
    attrs = Map.put(@valid_attrs, :past_performance_id, first_past_performance.id)
    {:ok, past_performance_item} =
      attrs
      |> Project.create_past_performance_item()
    assert {:ok, _ } = Project.update_past_performance_item(past_performance_item,
      %{attrs | details: "Details"})
  end

  test "fail update past performance item",  %{past_performances: [first_past_performance | _rest]} do
    attrs = Map.put(@valid_attrs, :past_performance_id, first_past_performance.id)
    {:ok, past_performance_item} =
      attrs
      |> Project.create_past_performance_item()
    assert {:error, _changeset } = Project.update_past_performance_item(past_performance_item,
      %{attrs | details: ""})
  end

  test "delete past performance item", %{past_performances: [first_past_performance | _rest]}  do
    attrs = Map.put(@valid_attrs, :past_performance_id, first_past_performance.id)
    {:ok, past_performance_item} =
      attrs
      |> Project.create_past_performance_item()
    assert {:ok, _} = Project.delete_past_performance_item(past_performance_item)
    assert_raise Ecto.NoResultsError, fn -> Project.get_past_performance_item_by_map!(%{id: past_performance_item.id}) end
  end

  defp gen_past_performances do
    Project.create_past_performance(%{name: "name1"})
    Project.list_past_performances
  end
end
