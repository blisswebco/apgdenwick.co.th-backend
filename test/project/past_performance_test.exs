defmodule Backend.Project.PastPerformanceTest do
  use Backend.DataCase, async: true

  alias Backend.Project.PastPerformance
  alias Backend.Project

  @valid_attrs %{name: "Past Performance Toward Public Sector"}
  @invalid_attrs_list [ %{}
		      ]

  test "changeset with valid attributes" do
    changeset = PastPerformance.changeset(%PastPerformance{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attrs" do
    Enum.map(@invalid_attrs_list,
      fn invalid_attrs ->
	changeset = PastPerformance.changeset(%PastPerformance{}, invalid_attrs)
	refute changeset.valid?
      end)
  end

  test "create_past_performance/1 with valid data creates a past_performance record" do
    assert {:ok, %PastPerformance{} = past_performance} = Project.create_past_performance(@valid_attrs)
    assert past_performance.name == @valid_attrs.name
  end

  test "duplicate pastperformance shall not be created" do
    {:ok, _} = Project.create_past_performance(@valid_attrs)
    assert {:error, _} = Project.create_past_performance(@valid_attrs)
  end

  test "success update past_performance" do
    {:ok, %PastPerformance{} = past_performance} = Project.create_past_performance(@valid_attrs)
    assert {:ok, updated_past_performance} = Project.update_past_performance(past_performance, %{name: "something else"})
    assert updated_past_performance.name == "something else"
    assert updated_past_performance.slug == past_performance.slug
  end

  test "fail update past_performance" do
    {:ok, %PastPerformance{} = past_performance} = Project.create_past_performance(@valid_attrs)
    assert {:error, _} = Project.update_past_performance(past_performance, %{name: ""})
  end

  test "delete past_performance" do
    {:ok, %PastPerformance{} = past_performance} = Project.create_past_performance(@valid_attrs)
    assert ^past_performance = Project.get_past_performance_by_map!(%{id: past_performance.id})
    assert {:ok, _} = Project.delete_past_performance(past_performance)
    assert_raise Ecto.NoResultsError, fn -> Project.get_past_performance_by_map!(%{id: past_performance.id}) end
  end
end
