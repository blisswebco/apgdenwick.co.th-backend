defmodule BackendWeb.PageControllerTest do
  use BackendWeb.ConnCase, async: true

  test "GET /", %{conn: conn} do
    assert_error_sent :not_found, fn ->
      get conn, "/"
    end
  end
end
