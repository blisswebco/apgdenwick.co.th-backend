defmodule BackendWeb.PastPerformanceItemApiControllerTest do
  use BackendWeb.ConnCase, async: true
  alias Backend.Project

  @past_performance_item_api_endpoint "/api/past_performance_items"

  setup do
    {:ok, past_performance} = %{name: "performance 1"} |> Project.create_past_performance
    Enum.map [1,2,3], fn number ->
      %{past_performance_id: past_performance.id, title: "title#{number}"}
      |> Project.create_past_performance_item
    end
  end

  test "GET #{@past_performance_item_api_endpoint}", %{conn: conn} do
    response = get conn, @past_performance_item_api_endpoint
    json_response(response, 200)
  end
end
