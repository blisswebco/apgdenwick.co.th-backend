defmodule BackendWeb.PastPerformanceApiControllerTest do
  use BackendWeb.ConnCase, async: true
  alias Backend.Project

  @past_performance_api_endpoint "/api/past_performances"
  @past_performance_list_params [
    %{name: "Past Performances Toward Government Sector"},
    %{name: "Past Performances Toward Private Sector"}
  ]
  

  setup do
    Enum.map(@past_performance_list_params, fn params ->
      Project.create_past_performance params
    end)
  end

  test "GET #{@past_performance_api_endpoint}", %{conn: conn} do
    response = get conn, @past_performance_api_endpoint
    json_response(response, 200)
  end
end
