defmodule BackendWeb.Api.ExecutiveSummaryControllerTest do
  use BackendWeb.ConnCase, async: true
  alias Backend.Staff

  @executive_summaries_api_endpoint "/api/executive_summaries"
  @executive_summaries_list_params [
    %{ "order" =>  1,
       "name" => "Mr. A B",
       "age" => 50,
       "gender" => "Male",
       "position" =>  "Director"
     },
    %{ "order" =>  2,
       "name" => "Mr. A B",
       "age" => 50,
       "gender" => "Male",
       "position" =>  "Director"
     }
  ]

  setup do
    executive_summaries = Enum.map(@executive_summaries_list_params, fn params ->
      {:ok, executive_summary} = Staff.create_executive_summary params
      executive_summary
    end)
    [first_executive_summary | remaining] = executive_summaries
    second_executvie_summary = hd remaining
    [executive_summaries: executive_summaries, first_executive_summary: first_executive_summary, second_executvie_summary: second_executvie_summary]
  end

  test "index /2", %{conn: conn} do
    response = get conn, @executive_summaries_api_endpoint
    json_response(response, 200)
  end
end
