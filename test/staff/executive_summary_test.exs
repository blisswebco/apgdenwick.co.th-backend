defmodule Backend.Staff.ExecutiveSummaryTest do
  use Backend.DataCase, async: true

  alias Backend.Staff.ExecutiveSummary
  alias Backend.Staff

  @valid_attrs_list (
    1..2
    |> Enum.map(fn(order) ->
      %{ "order" => order, "name" => "Name #{order}",
	 "age" => 50, "gender" => "Male",
	 "position" => "Manager"
       }
    end)
  )

  @invalid_attrs_list [ %{},
			hd(@valid_attrs_list) |> Map.delete(:order),
			hd(@valid_attrs_list) |> Map.delete(:name),
			hd(@valid_attrs_list) |> Map.delete(:age),
			hd(@valid_attrs_list) |> Map.delete(:gender),
			hd(@valid_attrs_list) |> Map.delete(:position)
		      ]
  @update_attrs %{ order: 100,
		   name: "Change Name",
		   age: 30,
		   gender: "Male",
		   position: "Position"
  }

  setup do
    [executive_summaries:
      @valid_attrs_list
      |> Enum.map(fn(valid_attrs) ->
           {:ok, executive_summary} = Staff.create_executive_summary(valid_attrs)
           executive_summary
         end)
    ]
  end

  test "valid attrs" do
    changeset = %ExecutiveSummary{} |> ExecutiveSummary.changeset( hd(@valid_attrs_list))
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    Enum.map(@invalid_attrs_list,
      fn invalid_attrs ->
	changeset = ExecutiveSummary.changeset(%ExecutiveSummary{}, invalid_attrs)
	changeset.valid?
      end)
  end

  test "executive summary age can't be string" do
    attrs = hd(@valid_attrs_list) |> Map.replace("age", "somestring")
    changeset = %ExecutiveSummary{} |> ExecutiveSummary.changeset(attrs)
    refute changeset.valid?
  end

  test "create executive summary with valid data" do
    attrs = hd(@valid_attrs_list) |> Map.replace("order", 3)
    assert {:ok, _} = Staff.create_executive_summary(attrs)
  end

  test "executive_summary with duplicate order shall not be created" do
    attrs = %{"order" => 2, "name" => "name", "age" => 50, "gender" => "male", "position" => "position"}
    assert {:error, %Ecto.Changeset{valid?: false}} = Staff.create_executive_summary(attrs)
  end

  test "update executive_summary success", %{executive_summaries: executive_summaries} do
    executive_summary = hd(executive_summaries)
    assert {:ok, updated_executive_summary} = Staff.update_executive_summary(executive_summary, @update_attrs)
    Enum.map([:order, :name, :age, :gender, :position], fn key ->
      assert @update_attrs[key] == Map.get(updated_executive_summary, key)
    end)
  end

  test "update executive summary with changes attrs which is invalid", %{executive_summaries: executive_summaries} do
    assert {:error, _changeset} = hd(executive_summaries) |> Staff.update_executive_summary(%{order: "some string"})
  end

  test "deleting an executive summary", %{executive_summaries: executive_summaries} do
    executive_summary = hd(executive_summaries)
    {:ok, _} = Staff.delete_executive_summary(executive_summary)
    assert_raise Ecto.NoResultsError, fn ->  Staff.get_executive_summary_by_map!(%{id: executive_summary.id}) end
  end
end
