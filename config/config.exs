# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :backend,
  ecto_repos: [Backend.Repo]

# Configures the endpoint
config :backend, BackendWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "A4GFxQvenlBqqovd6mbSL4tlP0PDrs+LMxKsAdECy6C5Q8VDCB1CrixBgMdcWRiZ",
  render_errors: [view: BackendWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Backend.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# ExAdmin

config :ex_admin,
  repo: Backend.Repo,
  module: BackendWeb,
  modules: [
    BackendWeb.ExAdmin.Dashboard,
    BackendWeb.ExAdmin.ExecutiveSummary,
    BackendWeb.ExAdmin.PastPerformance,
    BackendWeb.ExAdmin.PastPerformanceItem,
    BackendWeb.ExAdmin.User
  ],
  head_template: {BackendWeb.ExAdminView, "layout.html"}

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"


config :xain, :after_callback, {Phoenix.HTML, :raw}
