defmodule Backend.Repo.Migrations.RenameExecutiveSummaryEductionToEducation do
  use Ecto.Migration

  def up do
    rename table(:executive_summaries), :eduction, to: :education
  end

  def down do
    rename table(:executive_summaries), :education, to: :eduction    
  end
end
