defmodule Backend.Repo.Migrations.ChangeUserEducationToMoreInfo do
  use Ecto.Migration

  def change do
    alter table("executive_summaries") do
      add :other_info, :text, null: true
      remove(:education)
    end
  end
end
