defmodule Backend.Repo.Migrations.AddUser do
  use Ecto.Migration

  def change do
    create table("users") do
      add :username, :string, null: false
      add :crypted_password, :string, null: false
      timestamps()
    end

    create index("users", [:username], unique: true)
  end
end
