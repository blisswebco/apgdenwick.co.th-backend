defmodule Backend.Repo.Migrations.AddUniqueOnOrderExecutiveSummary do
  use Ecto.Migration

  def change do
    create index("executive_summaries", [:order], unique: true)
  end
end
