defmodule Backend.Repo.Migrations.AddExecutiveSummary do
  use Ecto.Migration

  def change do
    create table("executive_summaries") do
      add :order, :integer, null: false
      add :name, :string, null: false
      add :age, :integer, null: false
      add :gender, :string, null: false
      add :position, :string, null: false
      add :eduction, :text, null: false

      timestamps()
    end
  end
end
