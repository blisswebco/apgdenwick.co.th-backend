defmodule Backend.Repo.Migrations.ChangeItemTitleToDetails do
  use Ecto.Migration

  def up do
    rename table("past_performance_items"), :title, to: :details
  end

  def down do
    rename table("past_performance_items"), :details, to: :title
  end
end
