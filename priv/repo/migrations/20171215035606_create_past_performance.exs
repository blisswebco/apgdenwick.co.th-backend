defmodule Backend.Repo.Migrations.CreatePastPerformance do
  use Ecto.Migration

  def change do
    create table("past_performances") do
      add :name, :string, null: false
      add :slug, :string, null: false
      timestamps()
    end

    create index("past_performances", [:slug], unique: true)
    create index("past_performances", [:name], unique: true)
  end
end
