defmodule Backend.Repo.Migrations.AddPastPerformanceItem do
  use Ecto.Migration

  def change do
    create table("past_performance_items") do
      add :past_performance_id, references("past_performances")
      add :title, :text, null: false

      timestamps()
    end
  end
end
