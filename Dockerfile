FROM elixir:1.5.2
RUN mix local.hex --force
RUN mix local.rebar --force
RUN mkdir -p /backend
ENV MIX_ENV prod
ENV PORT 80
WORKDIR /backend
ADD . /backend
RUN mix deps.get --only prod
CMD mix phx.server
