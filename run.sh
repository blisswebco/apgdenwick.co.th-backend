#!/bin/bash

clear
mix deps.get --only-prod
MIX_ENV=prod mix phx.digest
MIX_ENV=prod PORT=80 mix phx.server
